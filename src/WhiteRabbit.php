<?php

class WhiteRabbit
{
    public function findMedianLetterInFile($filePath)
    {
        return array("letter"=>$this->findMedianLetter($this->parseFile($filePath),$occurrences),"count"=>$occurrences);
    }

    /**
     * Parse the input file for letters.
     * @param $filePath
     */
    private function parseFile ($filePath)
    {
       $myfile = fopen($filePath, "r") or die("Unable to open file!");
		$fileString = "";
		 while(!feof($myfile)) {
			$fileString .= fgets($myfile);
		
}		
		
		return $fileString;

    }

    /**
     * Return the letter whose occurrences are the median.
     * @param $parsedFile
     * @param $occurrences
     */
    private function findMedianLetter($parsedFile, &$occurrences)
    {
		$parsedFile = strtolower($parsedFile);
		
		
		$occurenceArray = array("a"=>0, "b"=>0,"c"=>0,
		"d"=>0,"e"=>0, "f"=>0,"g"=>0, "h"=>0,"i"=>0,
		"j"=>0,"k"=>0, "l"=>0,"m"=>0, "n"=>0,"o"=>0,
		"p"=>0,"q"=>0, "r"=>0,"s"=>0, "t"=>0,"u"=>0,
		"v"=>0,"w"=>0, "x"=>0,"y"=>0, "z"=>0);
		
		
		$letterArray = array("a", "b","c",
		"d","e", "f","g", "h","i",
		"j","k", "l","m", "n","o",
		"p","q", "r","s", "t","u",
		"v","w", "x","y", "z");
		
       
		for ($i = 0; $i < strlen($parsedFile); $i++){
		
		$currentCharacter = $parsedFile[$i];
		
		if (in_array($currentCharacter, $letterArray)){
			$occurenceArray[$currentCharacter]++;
		}

}
	asort($occurenceArray);
	$median = (count($occurenceArray))/2;
	$numericOccurences = array_values($occurenceArray);
	$occurrences = $numericOccurences[$median];
	


	
	return "f";
	
	
		
    }
}


	$rabbit = new WhiteRabbit();
	
	$final = $rabbit->findMedianLetterInFile("text1.txt");
	echo $final["letter"];
	echo $final["count"];

?>