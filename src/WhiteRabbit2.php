<?php

class WhiteRabbit2
{
    /**
     * return a php array, that contains the amount of each type of coin, required to fulfill the amount.
     * The returned array should use as few coins as possible.
     * The coins available for use is: 1, 2, 5, 10, 20, 50, 100
     * You can assume that $amount will be an int
     */
    public function findCashPayment($amount){
		
		$coinArray = array("1"=>0, "2"=>0, "5"=>0,
							"10"=>0, "20"=>0, "50"=>0, "100"=>0);
							
		$indexedKeys = array_keys($coinArray);
							
		/**Performing iterative steps, each time taking the modulo value of the highest 
		   coin which next in line. 
		*/
		
		$iterOne = intdiv($amount,100);
		$amount -= $iterOne*100;
		$iterTwo = intdiv($amount,50);
		$amount -= $iterTwo*50;
		$iterThree = intdiv($amount,20);
		$amount -= $iterThree*20;
		$iterFour = intdiv($amount,10);
		$amount -= $iterFour*10;
		$iterFive = intdiv($amount,5);
		$amount -= $iterFive*5;
		$iterSix = intdiv($amount,2);
		$amount -= $iterSix*2;
		$iterSeven = intdiv($amount,1);
		
		
		
		$coinAmount = array($iterSeven, $iterSix, $iterFive, $iterFour, $iterThree, $iterTwo, $iterOne);
		
		for ($x = 0; $x < count($coinArray); $x++){
			
		$coinArray[($indexedKeys[$x])] += $coinAmount[$x];
		
			
		}
		return $coinArray;
    }
	
		
}

		$rabbit = new WhiteRabbit2();
		
		$coins = $rabbit->findCashPayment(678);
		
		foreach ($coins as $key => $value) {
    echo "Key: $key; Value: $value\n";
}
?>